import UIKit

class GradientButton: Button {
    let gradientLayer = CAGradientLayer()
    var gradientColor: Color! {
        didSet {
            gradientLayer.colors = [gradientColor.start.cgColor, gradientColor.end.cgColor]
            layer.borderColor = gradientColor.start.cgColor
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        clipsToBounds = true
        layer.insertSublayer(gradientLayer, at: 0)
        addTarget(self, action: #selector(reverseGradient), for: [.touchDown, .touchUpInside, .touchDragExit])
    }
    
    @objc func reverseGradient() {
        gradientLayer.colors = gradientLayer.colors!.reversed()
    }
    
    override func layoutSubviews() {
        gradientLayer.frame = bounds
        updateRadius()
    }
    
    func updateRadius() {
        let rMax = min(bounds.size.width, bounds.size.height) / 2
        layer.cornerRadius = CGFloat(Settings.cornerRadiusCoef) * rMax
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        gradientColor = { gradientColor }()
    }
    
    func shake(_ amplitude: CGFloat) {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration     = 0.1
        shake.repeatCount  = 1
        shake.autoreverses = true
        shake.fromValue    = NSValue(cgPoint: CGPoint(x: center.x - amplitude, y: center.y))
        shake.toValue      = NSValue(cgPoint: CGPoint(x: center.x + amplitude, y: center.y))
        layer.add(shake, forKey: "position")
    }
}

class Button: UIButton {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        let label = titleLabel!
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        label.topAnchor.constraint(equalTo: topAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
