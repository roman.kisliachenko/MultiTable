import UIKit

class NumpadButton: GradientButton {
    @IBInspectable var isFirstRowBtn = false
    @IBInspectable var isSecondRowBtn = false
    @IBInspectable var isPrime = false
    lazy var isMainBtn = !(isFirstRowBtn || isSecondRowBtn)
    var color: Color! {
        didSet { update() }
    }
    var summary: Summary!
    private var amplitude: CGFloat!
    private var hasBorder = false
    private var question: Question!
    
    func update() {
        if let summary = summary {
            gradientColor = color
            question = summary.question
            let isForNumber2Btn: Bool
            switch summary.operation {
            case .addition:
                isEnabled = isMainBtn && tag <= 20
                isForNumber2Btn = isSecondRowBtn
            case .multiplication:
                isEnabled = isMainBtn && !isPrime
                isForNumber2Btn = isSecondRowBtn
                if isPrime { hasBorder = true }
            case .subtraction:
                isEnabled = isSecondRowBtn
                isForNumber2Btn = isMainBtn
            case .division:
                isEnabled = isSecondRowBtn
                isForNumber2Btn = isMainBtn
                if isPrime { hasBorder = true }
            }
            if isEnabled {
                isUserInteractionEnabled = true
                gradientColor = Color(.btnYellow)
            }
            if tag == question.number1 && isFirstRowBtn || tag == question.number2 && isForNumber2Btn {
                isEnabled = true
                isUserInteractionEnabled = false
                gradientColor = Color(.btnGreen)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addTarget(self, action: #selector(checkAnswer), for: .touchUpInside)
    }

    @objc func checkAnswer() {
        if tag == question.answer {
            summary.question = question.getNext()
        } else {
            question.isCorrect = false
            isEnabled = true
            isUserInteractionEnabled = false
            gradientColor = Color(.btnRed)
            shake(amplitude)
            if question.hared { summary.numpadTVC.update(image: nil) }
        }
    }
    
    override func layoutSubviews() {
        let stackViewWidth = superview!.bounds.width
        bounds.size.width = stackViewWidth / 11
        titleLabel!.font = titleLabel!.font.withSize(stackViewWidth / 22)
        if hasBorder { layer.borderWidth = stackViewWidth / 110 }
        amplitude = stackViewWidth / 55
        bounds.size.height = 0.9 * superview!.bounds.height
        super.layoutSubviews()
    }
}
