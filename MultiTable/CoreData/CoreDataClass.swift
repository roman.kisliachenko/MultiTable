import CoreData

@objc(Summary)
class Summary: NSManagedObject {
    static let container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MultiTable")
        container.loadPersistentStores() { _,_ in }
        print(NSPersistentContainer.defaultDirectoryURL())
        return container
    }()
    
    static func get(_ context: NSManagedObjectContext, _ rawOperation: String) -> Summary {
        let request = NSFetchRequest<Self>(entityName: entity().name!)
        request.predicate = NSPredicate(format: "rawOperation = %@", rawOperation)
        return (try! context.fetch(request) as [Summary]).first ?? Summary(context, rawOperation)
    }
    
    weak var numpadTVC: Updatable!
    var ab: MaxAB? {
        didSet {
            guard let new = ab, let old = oldValue else { return }
            if new.current == old.current { ab = nil }
            numpadTVC.update(ab?.max, animated: true)
        }
    }
    var points: (tortoise: Int, hare: Int)! {
        didSet {
            switch points! {
                case (5, 0):                 numpadTVC.update(image: .tortoise)
                case (0, 5):                 numpadTVC.update(image: .hare)
                case (0, 0), (1, 0), (0, 1): numpadTVC.update(image: nil)
                default: ()
            }
        }
    }
    weak var question: Question! {
        didSet {
            points = points.map { tortoise, hare in oldValue.accepted ? (0, hare + 1) : (tortoise + 1, 0) } ?? (0, 0)
            question.isCorrect = true
            question.hared = points.hare >= 5
            question.lastDate = Date()
            numpadTVC.update()
        }
    }
    lazy var operation = Operation(rawValue: rawOperation)!
    private lazy var thresholdTime: Double = {
        switch operation {
            case .addition:               return 2.5
            case .multiplication:         return 3.0
            case .subtraction, .division: return 2.0
        }
    }()
    private var epoch = [Question]()
    
    convenience init(_ context: NSManagedObjectContext, _ rawOperation: String) {
        self.init(context: context)
        self.rawOperation = rawOperation
        let q1 = Question(context, operation.getNumbers(3, 2))
        let q2 = Question(context, operation.getNumbers(2, 2))
        q2.lastDate = Date()
        addToQuestions(NSSet(objects: q1, q2))
    }
    
    func getQuestion() -> Question {
        epoch.popLast() ?? {
            epoch = newEpoch();
            return epoch.popLast()!
        }()
    }
    
    private func newEpoch() -> [Question] {
        var epoch = questions.filter() { $0.accepted == false }
        if epoch.isEmpty {
            updateCriticalTime()
            epoch = questions.filter() { $0.lastTime > criticalTime }
            epoch.forEach { $0.accepted = false }
            if let (a, b) = ab?.next() {
                epoch.insert(Question(self, operation.getNumbers(a, b)))
            }
        }
        var questions = self.questions.subtracting(epoch)
        if let question = question { questions.remove(question) }
        if let question = randomElement(questions.sorted() { $0.lastDate < $1.lastDate }) {
            question.accepted = false
            epoch.insert(question)
        }
        return epoch.sorted() { $0.lastDate > $1.lastDate }
    }
    
    private func updateCriticalTime() {
        let n = questions.count
        let meanTime = questions.reduce(0.0, { $0 + $1.lastTime }) / Double(n)
        let stddev = (questions.reduce(0.0, { $0 + pow(($1.lastTime - meanTime), 2) }) / Double(n)).squareRoot()
        criticalTime = max(meanTime + stddev, thresholdTime)
    }
    
    private func randomElement(_ questions: [Question]) -> Question? {
        let questions = questions.sorted() { $0.number1 + $0.number2 > $1.number1 + $1.number2 }
        let n = Int.random(in: 1 ... 1 + 3 * questions.count)
        return questions.prefix(n).randomElement()
    }
}

@objc(Question)
class Question: NSManagedObject {
    var isCorrect, hared: Bool!
    var answer: Int {
        summary.operation[number1, number2]
    }

    convenience init(_ summary: Summary, _ numbers: (Int, Int)) {
        self.init(summary.managedObjectContext!, numbers)
        self.summary = summary
    }
    
    convenience init(_ context: NSManagedObjectContext, _ numbers: (Int, Int)) {
        self.init(context: context)
        (number1, number2) = numbers
    }
    
    func getNext() -> Question {
        let context = managedObjectContext!
        let takeTime = abs(lastDate.timeIntervalSinceNow)
        accepted = !isCorrect ? false : takeTime < max(lastTime, summary.criticalTime) ? true : false
        addToDetails(Detail(context, takeTime, isCorrect, accepted, hared, showDate: lastDate))
        lastTime = takeTime
        try! context.save()
        return summary.getQuestion()
    }
}

@objc(Detail)
class Detail: NSManagedObject {
    convenience init(
        _ context: NSManagedObjectContext,
        _ takeTime: Double,
        _ isCorrect: Bool,
        _ accepted: Bool,
        _ hared: Bool,
        showDate: Date
    ) {
        self.init(context: context)
        self.takeTime = takeTime
        self.isCorrect = isCorrect
        self.accepted = accepted
        self.hared = hared
        self.showDate = showDate
    }
}
