import CoreData

extension Summary {
    @NSManaged var rawOperation: String
    @NSManaged var criticalTime: Double
    @NSManaged var questions: Set<Question>
    
    @objc(addQuestions:)
    @NSManaged func addToQuestions(_ values: NSSet)
}

extension Question {
    @NSManaged var number1: Int
    @NSManaged var number2: Int
    @NSManaged var lastTime: Double
    @NSManaged var accepted: Bool
    @NSManaged var lastDate: Date
    @NSManaged var summary: Summary
    @NSManaged var details: Set<Detail>
    
    @objc(addDetailsObject:)
    @NSManaged func addToDetails(_ value: Detail)
}

extension Detail {
    @NSManaged var takeTime: Double
    @NSManaged var isCorrect: Bool
    @NSManaged var accepted: Bool
    @NSManaged var hared: Bool
    @NSManaged var showDate: Date
    @NSManaged var question: Question
}
