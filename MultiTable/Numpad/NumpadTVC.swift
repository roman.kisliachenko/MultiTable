import UIKit

class NumpadTVC: TableViewController, Updatable {
    static var members = [NumpadTVC]() {
        didSet {
            guard members.count == Operation.allCases.count,
                  let lastNavCon = members.last!.navigationController else { return }
            members.enumerated().forEach { (order, numpadTVC) in numpadTVC.tab.order = order }
            lastNavCon.tabBarController!.viewControllers = members.map { $0.navigationController! }
        }
    }
    @IBOutlet var numpadBtns: [NumpadButton]!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var sign: UIImageView!
    lazy var mainNavCon = tabBarController!.navigationController!
    lazy var settStoryboard = UIStoryboard(name: "Settings", bundle: Bundle.main)
    private lazy var summary = Summary.get(context, tab.rawOperation)
    let context = Summary.container.newBackgroundContext()
    var tab: Tab
    
    init?(_ coder: NSCoder, _ tab: Tab) {
        self.tab = tab
        super.init(coder: coder)
        self.tab.numpadTVC = self
        Self.members.append(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        update(tab.color)
        setup(summary)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.navigationBar.barTintColor = tab.color.bar
        tabBarController!.tabBar.barTintColor = tab.color.bar
        sign.image = UIImage(tab.name.sign)
        mainNavCon.navigationBar.isHidden = true
    }
    
    func setup(_ summary: Summary) {
        numpadBtns.forEach { $0.summary = summary }
        summary.ab = MaxAB(summary.questions.count)
        update(summary.ab?.max, animated: false)
        summary.numpadTVC = self
        summary.question = summary.getQuestion()
    }
    
    @IBSegueAction
    func getStatTVC(_ coder: NSCoder) -> StatisticsTVC? {
        StatisticsTVC(coder, context, tab)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if sender != nil {
            for numpadTVC in Self.members.removed(at: tab.order) {
                numpadTVC.performSegue(withIdentifier: "showStatistics", sender: nil)
            }
        }
    }
    
    @IBAction func showSettings(_ sender: UIBarButtonItem) {
        let settTVC = settStoryboard.instantiateInitialViewController()!
        mainNavCon.pushViewController(settTVC, animated: false)
        mainNavCon.navigationBar.isHidden = false
    }
    
    @IBAction func reset(_ sender: UIBarButtonItem) {
        context.delete(summary)
        try! context.save()
        setup(Summary(context, tab.rawOperation))
    }
    
    func update() {
        numpadBtns.forEach { $0.update() }
    }
    
    func update(_ color: Color) {
        tableView.backgroundColor = color.bar
        numpadBtns.forEach { $0.color = color }
    }
    
    func update(image name: Icon.SystemName?) {
        guard let systemName = name else { return navigationItem.titleView = nil }
        navigationItem.titleView = UIImageView(image: UIImage(systemName, .large))
    }
    
    func update(_ digit: Int!, animated: Bool) {
        progressBar.isHidden = digit.map {
            progressBar.setProgress(Float($0-1)/8, animated: animated)
            return false
        } ?? true
    }
    
    required init?(coder: NSCoder) {
        fatalError("The class is not for storyboard.")
    }
}

protocol Updatable: AnyObject {
    func update()
    func update(_ color: Color)
    func update(image name: Icon.SystemName?)
    func update(_ digit: Int!, animated: Bool)
}
