import UIKit

struct Tab {
    let name: (tab: String, image: String, sign: Icon.SystemName)
    let rawOperation: String
    weak var numpadTVC: Updatable!
    var color: Color {
        didSet {
            numpadTVC.update(color)
            dict["color"] = color.value.rawValue
        }
    }
    var order: Int {
        didSet { dict["order"] = order }
    }
    @UserDefault private var dict: [String: Any]
    
    init(_ operation: Operation) {
        rawOperation = operation.rawValue
        let dictInitial: [String: Any]
        switch operation {
        case .addition:
            name = ("Addition", "plus", .plus)
            dictInitial = ["color": Color.Value.blue.rawValue, "order": 0]
        case .multiplication:
            name = ("Multiplication", "multiply", .multiply)
            dictInitial = ["color": Color.Value.brown.rawValue, "order": 1]
        case .subtraction:
            name = ("Subtraction", "minus", .plus)
            dictInitial = ["color": Color.Value.purple.rawValue, "order": 2]
        case .division:
            name = ("Division", "divide", .multiply)
            dictInitial = ["color": Color.Value.yellow.rawValue, "order": 3]
        }
        _dict = UserDefault(wrappedValue: dictInitial, key: rawOperation)
        let dict = _dict.wrappedValue
        color = Color(dict["color"] as! String)
        order = dict["order"] as! Int
    }
}

enum Operation: String, CaseIterable {
    case addition, multiplication
    case subtraction, division
    
    subscript(number1: Int, number2: Int) -> Int {
        switch self {
            case .addition:       return number1 + number2
            case .multiplication: return number1 * number2
            case .subtraction:    return number2 - number1
            case .division:       return number2 / number1
        }
    }
    
    func getNumbers(_ a: Int, _ b: Int) -> (Int, Int) {
        switch self {
            case .addition, .multiplication: return (a, b)
            case .subtraction:               return (a, a + b)
            case .division:                  return (a, a * b)
        }
    }
}

struct Color {
    let value: Value
    let bar, content: UIColor?
    let start, end: UIColor
    
    init(_ color: Value) {
        value = color
        let rawColor = color.rawValue
        bar =     UIColor(named: "\(rawColor)/bar")
        content = UIColor(named: "\(rawColor)/content")
        start =   UIColor(named: "\(rawColor)/start")!
        end =     UIColor(named: "\(rawColor)/end")!
    }
    
    init(_ rawColor: String) {
        self.init(Value(rawValue: rawColor)!)
    }
    
    enum Value: String {
        case btnYellow, btnGreen, btnRed
        case silver, brown, yellow, cyan, blue, purple, pink
    }
}

enum Icon {
    case emoji(String)
    case sfSymbol(SystemName)
    
    enum SystemName: String {
        case plus, multiply
        case square, circle
        case tortoise, hare
        case sum, timer
    }
}

struct MaxAB: IteratorProtocol {
    private(set) var current = (2, 2)
    private(set) var max = 2
    private let nLimit = 64
    private var nUsed = 1
    
    init?(_ n: Int) {
        guard 0 < n && n < nLimit else { return nil }
        while nUsed < n { _ = next() }
    }
    
    mutating func next() -> (Int, Int)? {
        guard nUsed < nLimit else { return nil }
        var (a, b) = current
        if a == b {
            a += 1
            b = 2
        } else {
            (a, b) = (b, a)
            if a > b { b += 1 }
        }
        nUsed += 1
        current = (a, b)
        max = Swift.max(a, b)
        return (a, b)
    }
}

class TableViewController: UITableViewController {
    var rowHeightIsSetted: Bool? {
        willSet {
            if rowHeightIsSetted == nil {
                tableView.setAppropriateRowHeight()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        rowHeightIsSetted = true
    }
}
