import UIKit

extension Array {
    func removed(at index: Int) -> Array {
        var array = self
        array.remove(at: index)
        return array
    }
}

extension Double {
    func toString(numberStyle: NumberFormatter.Style) -> String {
        let nf = NumberFormatter()
        if numberStyle == .decimal {
            nf.minimumFractionDigits = 1
            nf.maximumFractionDigits = 1
        }
        nf.numberStyle = numberStyle
        return nf.string(from: NSNumber(value: self)) ?? ""
    }
}

extension UIView {
    func subviews<T: UIView>(ofType type: T.Type) -> [T] {
        var result = subviews.compactMap { $0 as? T }
        subviews.forEach { view in result.append(contentsOf: view.subviews(ofType: type)) }
        return result
    }
    
    func superview<T: UIView>(ofType type: T.Type) -> T? {
        superview as? T ?? superview?.superview(ofType: type)
    }
}

extension UITableView {
    private var numberOfRows: Int {
        (0 ..< numberOfSections).reduce(0) { number, index in number + numberOfRows(inSection: index) }
    }
    private var sectionsHeaderFooterHeight: CGFloat {
        (0 ..< numberOfSections).reduce(0) { height, index in
            height + rectForHeader(inSection: index).height + rectForFooter(inSection: index).height }
    }
    
    func setAppropriateRowHeight() {
        rowHeight = (frame.height - sectionsHeaderFooterHeight) / CGFloat(numberOfRows)
    }
}

extension UITabBarController: UITabBarControllerDelegate {
    public func tabBarController(
        _ tabBarController: UITabBarController,
        shouldSelect viewController: UIViewController
    ) -> Bool {
        tabBarController.selectedViewController == viewController ? false : true
    }
}

extension UIImage {
    convenience init?(
        _ systemName: Icon.SystemName,
        fill: Bool = false,
        _ scale: UIImage.SymbolScale = .unspecified
    ) {
        let name = systemName.rawValue + (fill ? ".fill" : "")
        self.init(systemName: name, withConfiguration: UIImage.SymbolConfiguration(scale: scale))
    }
}
