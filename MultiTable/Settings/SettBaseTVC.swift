import UIKit

class SettBaseTVC: TableViewController {
    let color = Color(.silver)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController!.navigationBar.barTintColor = color.bar
        tableView.backgroundColor = color.content
        navigationItem.hidesBackButton = true
    }

    @IBAction func closeSettings(_ sender: UIBarButtonItem) {
        navigationController!.popViewController(animated: false)
    }
    
    override func tableView(
        _ tableView: UITableView,
        willDisplayHeaderView view: UIView,
        forSection section: Int
    ) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.tintColor = color.bar
        let textLabel = headerView.textLabel!
        textLabel.textAlignment = .center
    }
    
    override func tableView(
        _ tableView: UITableView,
        willDisplay cell: UITableViewCell,
        forRowAt indexPath: IndexPath
    ) {
        cell.backgroundColor = color.content
        cell.contentView.backgroundColor = color.content
    }
}

@propertyWrapper
struct UserDefault<T> {
    private let key: String
    private var storedValue: T
    var wrappedValue: T {
        get { storedValue }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
            storedValue = newValue
        }
    }
    
    init(wrappedValue: T, key: String) {
        storedValue = UserDefaults.standard.object(forKey: key) as? T ?? wrappedValue
        self.key = key
    }
}

enum Settings {
    @UserDefault(key: "selectedIndex")    static var selectedIndex: Int = 0
    @UserDefault(key: "cornerRadiusCoef") static var cornerRadiusCoef: Float = 0.3
    @UserDefault(key: "thresholdTH")      static var thresholdTH: Int = 5
    @UserDefault(key: "dayGoalTime")      static var dayGoalTime: Int = 10
}
