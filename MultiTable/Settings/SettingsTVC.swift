import UIKit

class SettingsTVC: SettBaseTVC {
    @IBInspectable var movableRowsSection = -1
    @IBOutlet weak var slider: UISlider!
    lazy var gradientButtons = tableView.subviews(ofType: GradientButton.self)
    let colorStack = [Color(.brown), Color(.yellow), Color(.cyan), Color(.blue), Color(.purple), Color(.pink)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isEditing = true
        setSliderConfig()
    }
    
    override func tableView(
        _ tableView: UITableView,
        willDisplay cell: UITableViewCell,
        forRowAt indexPath: IndexPath
    ) {
        super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
        guard indexPath.section == movableRowsSection else { return }
        let movableCell = cell as! TableViewCell
        let tab = NumpadTVC.members[indexPath.row].tab
        movableCell.isSpecial = (tab.numpadTVC as! NumpadTVC).tabBarController!.selectedIndex == indexPath.row
        movableCell.name = tab.name.tab
        for (button, color) in zip(cell.subviews(ofType: GradientButton.self), colorStack) {
            button.gradientColor = color
            if color.value == tab.color.value {
                button.isHidden = false
                if movableCell.isSpecial { button.layer.borderWidth = cell.bounds.width / 75 }
                button.titleLabel!.text = tab.name.tab
            }
        }
    }
    
    @IBAction func colorAction(_ sender: UIButton) {
        let cell = sender.superview(ofType: TableViewCell.self)!
        let buttons = cell.subviews(ofType: GradientButton.self)
        let borderWidth = cell.bounds.width / 75
        let imageButton = cell.contentView.subviews.first!
        if sender == imageButton {
            imageButton.isHidden = true
            for button in buttons {
                if button.isHidden { button.isHidden = false }
                else { button.layer.borderWidth = borderWidth }
                button.isUserInteractionEnabled = true
                button.titleLabel!.text = ""
            }
        } else {
            imageButton.isHidden = false
            for button in buttons {
                button.isHidden = button != sender
                button.layer.borderWidth = 0.0
                button.isUserInteractionEnabled = false
            }
            if cell.isSpecial { sender.layer.borderWidth = borderWidth }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.125) { sender.titleLabel!.text = cell.name }
            NumpadTVC.members[tableView.indexPath(for: cell)!.row].tab.color = (sender as! GradientButton).gradientColor
        }
    }
    
    @IBAction func radiusDidChange(_ sender: UISlider) {
        Settings.cornerRadiusCoef = sender.value
        gradientButtons.forEach { $0.updateRadius() }
    }
    
    override func tableView(
        _ tableView: UITableView,
        targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath,
        toProposedIndexPath proposedDestinationIndexPath: IndexPath
    ) -> IndexPath {
        guard proposedDestinationIndexPath.section == movableRowsSection else {
            let returnRow = proposedDestinationIndexPath.section > movableRowsSection ? NumpadTVC.members.count - 1 : 0
            return IndexPath(row: returnRow, section: movableRowsSection)
        }
        return proposedDestinationIndexPath
    }
    
    override func tableView(
        _ tableView: UITableView,
        moveRowAt sourceIndexPath: IndexPath,
        to destinationIndexPath: IndexPath
    ) {
        guard sourceIndexPath != destinationIndexPath else { return }
        let movedNumpadTVC = NumpadTVC.members.remove(at: sourceIndexPath.row)
        NumpadTVC.members.insert(movedNumpadTVC, at: destinationIndexPath.row)
    }
    
    override func tableView(
        _ tableView: UITableView,
        canMoveRowAt indexPath: IndexPath
    ) -> Bool {
        indexPath.section == movableRowsSection ? true : false
    }
    
    override func tableView(
        _ tableView: UITableView,
        editingStyleForRowAt indexPath: IndexPath
    ) -> UITableViewCell.EditingStyle {
        .none
    }

    override func tableView(
        _ tableView: UITableView,
        shouldIndentWhileEditingRowAt indexPath: IndexPath
    ) -> Bool {
        false
    }
    
    func setSliderConfig() {
        slider.minimumValueImage = UIImage(.square, .large)
        slider.maximumValueImage = UIImage(.circle, .large)
        let thumbImage = UIImage(.circle, fill: true, .small)
        slider.setThumbImage(thumbImage, for: .normal)
        slider.setThumbImage(thumbImage, for: .highlighted)
        slider.value = Settings.cornerRadiusCoef
    }
}

class TableViewCell: UITableViewCell {
    var isSpecial = false
    var name = ""
}
