import UIKit
import CoreData

class StatisticsTVC: StatBaseTVC {
    @IBOutlet var statLabels: [UILabel]!
    @IBOutlet var valueLabels: [UILabel]!
    let icons: [Icon] = [.emoji("✅"), .emoji("⚠️"), .emoji("🚫"), .sfSymbol(.sum), .sfSymbol(.timer)]
    let context: NSManagedObjectContext
    var valueTexts: [String] {
        var (accepted, tooSlow, incorrect, spentTime) = (0, 0, 0, 0.0)
        
        let countED = NSExpressionDescription()
        countED.name = "number"
        countED.expressionResultType = .integer32AttributeType
        countED.expression = NSExpression(format: "count:(showDate)")
        
        let timeED = NSExpressionDescription()
        timeED.name = "time"
        timeED.expressionResultType = .doubleAttributeType
        timeED.expression = NSExpression(format: "sum:(takeTime)")
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Detail.entity().name!)
        request.predicate = NSPredicate(format: "question.summary.rawOperation = %@", tab.rawOperation)
        request.resultType = .dictionaryResultType
        request.propertiesToGroupBy = ["isCorrect", "accepted"]
        request.propertiesToFetch = ["isCorrect", "accepted", countED, timeED]
        
        (try! context.fetch(request) as! [[String: AnyObject]]).forEach { dict in
            let isCorrectAccepted = (dict["isCorrect"], dict["accepted"]) as! (Bool, Bool)
            let number = dict["number"] as! Int
            spentTime += dict["time"] as! Double
            switch isCorrectAccepted {
                case (true, true):      accepted = number
                case (true, false):     tooSlow = number
                case (false, false):    incorrect = number
                default: break
            }
        }
        let overAll = accepted + tooSlow + incorrect
        guard overAll > 0 else { return ["0", "0", "0", "0", ""] }
        var valueTexts = [accepted, tooSlow, incorrect].map { value -> String in
            let percentage = Double(value) / Double(overAll)
            return "\(value)  (" + percentage.toString(numberStyle: .percent) + ")"
        }
        valueTexts.append(String(overAll))
        let meanTime = spentTime / Double(overAll)
        valueTexts.append(meanTime.toString(numberStyle: .decimal) + " sec")
        return valueTexts
    }
    
    init?(_ coder: NSCoder, _ context: NSManagedObjectContext, _ tab: Tab) {
        self.context = context
        super.init(coder, tab)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard Self.isStatsNeeded else { return }
        zip(valueLabels, valueTexts).forEach { (label, value) in label.text = value }
        populateIcons()
    }
    
    func populateIcons() {
        zip(icons, statLabels).forEach { (icon, label) in
            let text = "\(label.text!):"
            switch icon {
            case .emoji(let emoji):
                label.text = [emoji, text].joined(separator: " ")
            case .sfSymbol(let systemName):
                let attrText = NSMutableAttributedString(attributedString: NSAttributedString(
                    attachment: NSTextAttachment(image: UIImage(systemName, .large)!)
                ))
                attrText.append(NSAttributedString(string: " \(text)"))
                label.attributedText = attrText
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("The class is not for storyboard.")
    }
}
