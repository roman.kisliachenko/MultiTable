import UIKit

class StatBaseTVC: TableViewController {
    static var isStatsNeeded = true
    let tab: Tab
    let color = Color(.silver)
    
    init?(_ coder: NSCoder, _ tab: Tab) {
        self.tab = tab
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController!.navigationBar.barTintColor = color.bar
        navigationItem.hidesBackButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController!.tabBar.barTintColor = tab.color.bar
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            Self.isStatsNeeded.toggle()
            for numpadTVC in NumpadTVC.members.removed(at: tab.order) {
                let navCon = numpadTVC.navigationController!
                navCon.topViewController!.view.removeFromSuperview()
                navCon.popViewController(animated: false)
            }
            Self.isStatsNeeded.toggle()
        }
    }
    
    @IBAction func closeStatistics(_ sender: UIBarButtonItem) {
        navigationController!.popViewController(animated: true)
    }
    
    override func tableView(
        _ tableView: UITableView,
        willDisplayHeaderView view: UIView,
        forSection section: Int
    ) {
        if section == 1 {
            let headerView = view as! UITableViewHeaderFooterView
            headerView.tintColor = tab.color.bar
            let textLabel = headerView.textLabel!
            textLabel.textAlignment = .center
            textLabel.text = tab.name.tab
        }
    }
    
    override func tableView(
        _ tableView: UITableView,
        willDisplay cell: UITableViewCell,
        forRowAt indexPath: IndexPath
    ) {
        switch indexPath.section {
            case 0: cell.contentView.backgroundColor = color.content
            case 1: cell.contentView.backgroundColor = tab.color.content
            default: break
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("The class is not for storyboard.")
    }
}
